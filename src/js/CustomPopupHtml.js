import placeholderImage from "@assets/placeholder.png";
import SimpleScrollbar from "simple-scrollbar";
import { DomUtil } from "leaflet";
import { PopupHtmlBase } from "./LeafletCategoryMap/index.js";

export const CustomPopupHtml = PopupHtmlBase.extend({
  _createHMTL: function () {
    const content = DomUtil.create("div", "map-popup-content");
    const bodyWrapper = DomUtil.create("div", "map-popup-body-wrapper");
    const body = this._createBody();
    const header = this._createHeader();
    bodyWrapper.appendChild(body);
    content.appendChild(header);
    content.appendChild(bodyWrapper);

    const category = this._getCategory();
    // add category class to content div to allow category-wise css modifications
    if (typeof category !== "undefined") {
      DomUtil.addClass(content, category);
    }

    return content;
  },

  _createHeader: function () {
    const header = DomUtil.create("div", "map-popup-header");
    const img = this._createHeaderImage();

    header.append(img);

    return header;
  },

  _createBody: function () {
    const headline = DomUtil.create("h3");
    headline.innerHTML = this._getProp("headline");

    const meta = this._createMetaBlock();

    const abstract = DomUtil.create("p");
    abstract.innerHTML = this._getProp("abstract");

    const leadParagraph = DomUtil.create("p");
    leadParagraph.innerHTML = this._getProp("leadParagraph");

    const body = DomUtil.create("div", "map-popup-body");
    body.appendChild(headline);
    body.appendChild(meta);
    body.appendChild(abstract);
    body.appendChild(leadParagraph);

    SimpleScrollbar.initEl(body);

    return body;
  },

  _createMetaBlock: function () {
    const date = new Date(this._getProp("pubDate"));
    const meta = DomUtil.create("p", "map-popup-description-meta");
    meta.innerHTML = date.toLocaleDateString();
    meta.innerHTML = meta.innerHTML + "<br/>" + this._getProp("location");
    meta.innerHTML = meta.innerHTML + "<br/>" + this._getProp("category");
    meta.innerHTML =
      meta.innerHTML +
      `<br/><a href="${this._getProp("webUrl")}">NY Times link</a> (paywall 😞)`;

    return meta;
  },

  _createHeaderImage: function () {
    const imageUrl = this._getProp("imageUrl");

    const image = new Image();

    if (imageUrl) {
      image.src = imageUrl;
    } else {
      image.src = placeholderImage;
    }

    image.className = "map-header-image";
    return image;
  },
});

export const customPopupHtml = (options) => new CustomPopupHtml(options);
