"use strict";
import { categoryMap } from "./LeafletCategoryMap/index.js";
import "@styles/main/main.scss";
import "@styles/custom/custom.scss";
import { ApiClient } from "./ApiClient.js";
import { options } from "./config.js";

const client = new ApiClient();

const map = categoryMap("leaflet-category-demo", options);
client.get("/api/articles", (data) => map.addMarkerLayer(data));
