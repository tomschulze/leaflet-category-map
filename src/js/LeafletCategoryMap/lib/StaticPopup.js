import { Class, Util, DomEvent, DomUtil } from "leaflet";

export const StaticPopup = Class.extend({
  options: {
    id: "staticPopup",
    noPopupText: "",
  },

  initialize: function (options) {
    Util.setOptions(this, options);
  },

  _close: function () {
    var staticPopup = DomUtil.get(this.options.id);
    staticPopup.innerHTML = "";
    staticPopup.setAttribute("style", "display:none");
  },

  addTo: function (map) {
    this.onAdd(map);
  },

  onAdd: function (map) {
    this._map = map;
  },

  applyTo: function (layer) {
    // Trigger empty contents when the script
    // has loaded on the page.
    this._close();
    this._layer = layer;

    this._map.on("click", this._close, this);
    // Listen for individual marker clicks.
    layer.on("click", this._featureClick, this);
  },

  _setPopupHTML: function (eventTarget) {
    // method gets called on every click on marker
    var target = eventTarget;

    var closeButton = DomUtil.create("a", "map-popup-close-button");
    closeButton.innerHTML = "&#215;";
    closeButton.setAttribute("id", "map-popup-close-button");
    DomEvent.on(closeButton, "click", this._close, this);

    var popupContent;
    if (typeof target._popup._content == "string") {
      popupContent = target._popup._content;
    } else {
      popupContent = target._popup.getContent();
    }

    var staticPopup = DomUtil.get(this.options.id);
    staticPopup.innerHTML = ""; // clear static popup content

    var isCloseButton = document.getElementById("map-popup-close-button");
    if (isCloseButton === null) {
      popupContent.insertBefore(closeButton, popupContent.firstChild);
    }

    staticPopup.appendChild(popupContent);
    staticPopup.setAttribute("style", "display:flex;"); // toggle visibility;
  },

  _featureClick: function (e) {
    var target;
    // choose a target
    if (e.layer) {
      target = e.layer;
    } else {
      target = e.target;
    }
    // Force the popup closed.
    target.closePopup();
    this._setPopupHTML(target);
  },

  onRemove: function () {
    console.log("remove layer");
    this._map.off({ click: this._close }, this);
    this._layer.off({ click: this._featureClick }, this);

    this._map = null;
    this._layer = null;
  },
});

export const staticPopup = (options) => new StaticPopup(options);

/*
The MIT License (MIT)

Copyright (c) 2015 Karsten Hinz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

// https://github.com/kartenkarsten/leaflet-staticPopup
