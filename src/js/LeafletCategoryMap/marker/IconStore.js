import markerSvg from "@assets/marker.svg";
import { Class, divIcon, setOptions } from "leaflet";

export const IconStore = Class.extend({
  _icons: null,
  _categories: null,
  options: {
    categoryColorMap: {},
    defaultColor: "rgb(128,128,128)",
    //passed to L.DivIcon options
    iconOptions: {
      iconSize: [42, 55],
      iconAnchor: [21, 55],
      popupAnchor: [1, -24],
      //html is overriden
      //className is overriden
    },
    iconSvg: markerSvg,
  },

  initialize: function (options) {
    if (typeof options !== "undefined") {
      setOptions(this, options);
    }

    this._initCategories();
    this._initIcons();
  },

  getIcon: function (category) {
    if (Object.hasOwn(this._icons, category)) {
      return this._icons[category];
    }

    return this._defaultIcon;
  },

  isInitialized: function () {
    return this._icons !== null;
  },

  _initCategories: function () {
    this._categories = Object.keys(this.options.categoryColorMap);
  },

  _initIcons: function () {
    const categories = this._categories;
    this._icons = categories
      .map((category) => [category, this._generateIcon(category)])
      .reduce((a, b) => {
        const [category, icon] = b;
        a[category] = icon;
        return a;
      }, {});
    this._defaultIcon = this._generateIcon();
  },

  _generateIcon: function (category) {
    const options = this.options.iconOptions;
    options.html = this._setSVG(category);

    if (typeof category === "string") {
      // TODO needs to be "cssified"/sanitized correctly
      options.className = category.replace("_", "-");
    } else {
      options.className = "default";
    }

    return divIcon(options);
  },

  _setSVG: function (category) {
    const svg = this.options.iconSvg;
    const color = this._getColorForCategory(category);
    const parser = new DOMParser();

    const parsedSvg = parser.parseFromString(svg, "image/svg+xml");
    parsedSvg.documentElement
      .getElementById("outer")
      .setAttribute("fill", color);
    return parsedSvg.documentElement.outerHTML;
  },

  _getColorForCategory: function (category) {
    const colorMap = this.options.categoryColorMap;

    if (Object.hasOwn(colorMap, category)) {
      return colorMap[category];
    }

    return this.options.defaultColor;
  },
});

export const iconStore = (options) => new IconStore(options);
