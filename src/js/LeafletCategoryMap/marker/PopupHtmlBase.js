import { Class } from "leaflet";

export const PopupHtmlBase = Class.extend({
  _props: {},
  _category: null,

  initialize: function () {
    if (typeof this._createHMTL !== "function") {
      throw new Error(
        "You must extend the class 'PopupHtmlBase' and provide the method '_createHTML()'",
      );
    }
  },

  buildHTML: function ({ props, category }) {
    this._props = props;
    this._category = category;

    const html = this._createHMTL();

    this._reset();
    return html;
  },

  _reset: function () {
    this._props = {};
    this.category = null;
  },

  _getCategory: function () {
    return this._category;
  },

  _getProp: function (key) {
    if (Object.hasOwn(this._props, key)) {
      return this._props[key];
    }

    return "";
  },
});
