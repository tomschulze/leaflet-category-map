import { Marker, setOptions, Popup } from "leaflet";

export const CategoryMarker = Marker.extend({
  options: {
    iconStore: null,
    popupHtml: null,
    extractors: null,
  },
  _data: null,

  initialize: function (data, options) {
    if (typeof options !== "undefined") {
      setOptions(this, options);
    }

    this._data = data;
    this._validate();

    const { category, latlng, props } = this._extract(data);
    const icon = this.options.iconStore.getIcon(category);
    const popup = new Popup({ autoPan: false });
    const popupContent = this.options.popupHtml.buildHTML({ props, category });
    popup.setContent(popupContent);

    Marker.prototype.initialize.call(this, latlng, { category, icon });
    this.bindPopup(popup);
  },

  _validate: function () {
    if (this._data == null) {
      throw new Error("no data provided");
    }

    if (this.options.iconStore == null) {
      throw new Error("no icon store provided");
    }

    // TODO decide whether this is necessary
    if (!this.options.iconStore.isInitialized()) {
      throw new Error("IconStore has not been initialized yet.");
    }

    if (this.options.popupHtml == null) {
      throw new Error("no popupHtml builder provided");
    }

    if (this.options.extractors == null) {
      throw new Error("no extractors provided");
    }
  },

  _extract(data) {
    const { categoryExtractor, propsExtractor, coordinateExtractor } =
      this._getExtractors();

    return {
      latlng: coordinateExtractor(data),
      category: categoryExtractor(data),
      props: propsExtractor(data),
    };
  },

  _getExtractors() {
    return this.options.extractors;
  },
});

export const categoryMarker = (data, options) =>
  new CategoryMarker(data, options);
