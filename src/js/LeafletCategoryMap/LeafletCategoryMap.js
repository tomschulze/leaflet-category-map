import { Map, DomUtil } from "leaflet";
import { MarkerClusterGroup } from "leaflet.markercluster";
import { GestureHandling } from "leaflet-gesture-handling";

import { staticPopup } from "./lib/StaticPopup.js";
import { topoJson } from "./lib/leaflet.topojson.js";

import { categoryControl, zoomControl } from "./controls/index.js";
import { categoryMarker, iconStore } from "./marker/index.js";

export const CategoryMap = Map.extend({
  _clusterOptions: null,
  _entryPointId: null,
  _leafletOptions: null,
  _mapId: "world-map",
  _staticPopup: null,
  _staticPopupId: "leaflet-static-popup",
  _svgOptions: null,
  _topoJson: null,
  _extractors: null,
  _popupHtml: null,
  _categoryOptions: {},

  initialize: function (entryPointId, options) {
    const {
      topoJsonData,
      leafletOptions,
      svgOptions,
      clusterOptions,
      categoryOptions,
      extractors,
      popupHtml,
    } = options;

    this._entryPointId = entryPointId;
    this._leafletOptions = leafletOptions;
    this._clusterOptions = clusterOptions;
    this._extractors = extractors;
    this._popupHtml = popupHtml;
    this._categoryOptions = categoryOptions;
    this._topoJson = topoJsonData;
    this._svgOptions = svgOptions;

    this._createDOM();
    Map.prototype.initialize.call(this, this._mapId, this._leafletOptions);

    this.addHandler("gestureHandling", GestureHandling);

    this._staticPopup = staticPopup({ id: this._staticPopupId });
    this._staticPopup.addTo(this);

    topoJson(this._topoJson, { style: () => this._svgOptions }).addTo(this);

    zoomControl({
      center: this._leafletOptions.center,
      zoom: this._leafletOptions.zoom,
    }).addTo(this);
  },

  _createDOM: function () {
    const wrapper = DomUtil.create("div", "map-wrapper");

    const mapContainer = DomUtil.create("div", "map-container");
    mapContainer.setAttribute("id", this._mapId);
    const staticPopupContainer = DomUtil.create("div", "leaflet-static-popup");
    staticPopupContainer.setAttribute("id", this._staticPopupId);

    wrapper.append(mapContainer, staticPopupContainer);

    const entryPoint = DomUtil.get(this._entryPointId);
    entryPoint.append(wrapper);
  },

  addMarkerLayer: function (data) {
    const clusterLayer = new MarkerClusterGroup(this._clusterOptions);
    this.addLayer(clusterLayer);

    const categoryMarkerOptions = {
      iconStore: iconStore({
        defaultColor: this._categoryOptions.defaultColor,
        categoryColorMap: this._categoryOptions.colorMap,
      }),
      popupHtml: this._popupHtml,
      extractors: this._extractors,
    };

    const categories = Object.keys(this._categoryOptions.colorMap);
    const { categoryExtractor } = this._extractors;

    const markers = data
      .filter((datapoint) => {
        const category = categoryExtractor(datapoint);
        if (categories.includes(category)) {
          return true;
        }

        return false;
      })
      .map((datapoint) => {
        const marker = categoryMarker(datapoint, categoryMarkerOptions);
        this._staticPopup.applyTo(marker);
        return marker;
      });

    clusterLayer.addLayers(markers);

    categoryControl({
      layer: clusterLayer,
      categories,
      categoryOptions: this._categoryOptions,
    }).addTo(this);
  },
});

export const categoryMap = (entryPointId, options) =>
  new CategoryMap(entryPointId, options);
