export const CategoryControlButton = L.Control.EasyButton.extend({
  options: {
    categoryName: null,
    clusterLayer: null,
    categoryLayers: null,
    states: { visible: "visible", hidden: "hidden" },
    cssCategoryName: null,
    color: null,
  },

  initialize: function ({ categoryName, clusterLayer, categoryLayers, color }) {
    // TODO needs to be "cssified"/sanitized correctly
    const cssCategoryName = categoryName.replace("_", "-");

    L.setOptions(this, {
      categoryName,
      clusterLayer,
      categoryLayers,
      cssCategoryName,
      color,
    });

    let states = Object.keys(this.options.states);
    states = states.map((stateName) =>
      this._createEasyButtonState({ stateName }),
    );

    const tagName = "a";

    L.Control.EasyButton.prototype.initialize.call(this, { tagName, states });
  },

  _stateCallbacks: function () {
    const _clusterLayer = this.options.clusterLayer;
    const _categoryLayers = this.options.categoryLayers;
    const _states = this.options.states;

    return {
      visible(btn) {
        _clusterLayer.removeLayers(_categoryLayers);
        btn.state(_states.hidden);
      },
      hidden(btn) {
        _clusterLayer.addLayers(_categoryLayers);
        btn.state(_states.visible);
      },
    };
  },

  _createEasyButtonState: function ({ stateName }) {
    const _categoryName = this.options.categoryName;
    const _stateCallbacks = this._stateCallbacks();
    const iconHTML = this._createIconHTML({ stateName });

    return {
      stateName: stateName,
      icon: iconHTML,
      title: `${_categoryName}-${stateName}`,
      onClick: _stateCallbacks[stateName],
    };
  },

  _createIconHTML: function ({ stateName }) {
    const color = this.options.color;
    const cssCategoryName = this.options.cssCategoryName;
    if (stateName == "visible") {
      return `<div style="background-color:${color}" class="easy-btn easy-btn-category ${cssCategoryName}-${stateName}"></div>`;
    }
    return `<div style="box-sizing:border-box; border:2px solid ${color}" class="easy-btn easy-btn-category ${cssCategoryName}-${stateName}"></div>`;
  },
});

export const categoryControlButton = (options) =>
  new CategoryControlButton(options);
