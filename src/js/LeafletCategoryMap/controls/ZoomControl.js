import compassSvg from "@assets/compass.svg";

export const ZoomControl = L.Control.EasyButton.extend({
  options: {
    stateName: "zoom-out",
    title: "Zoom out",
  },

  initialize: function ({ center, zoom }) {
    const icon = `<div class="easy-btn state-${this.options.stateName}">${compassSvg}</div>`;
    const onClick = (_, map) => {
      map.setView(center, zoom);
    };
    const { stateName, title } = this.options;

    const states = [
      {
        icon,
        onClick,
        stateName,
        title,
      },
    ];

    const tagName = "a";

    L.Control.EasyButton.prototype.initialize.call(this, { tagName, states });
  },
});

export const zoomControl = (options) => new ZoomControl(options);
