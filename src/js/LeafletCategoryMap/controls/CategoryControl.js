import { categoryControlButton } from "./CategoryControlButton.js";

export const CategoryControl = L.Control.EasyBar.extend({
  options: {
    layer: L.layerGroup([]), // must inherit from layerGroup
    categories: [],
    categoryKey: "category",
    fallbackCategoryName: "other",
    categoryOptions: {
      defaultColor: "rgb(128,128,128)",
      colorMap: {},
    },
  },

  _layersByCategory: null,
  _buttons: null,

  initialize: function ({ layer, categories, categoryOptions }) {
    L.setOptions(this, { layer, categories, categoryOptions });

    this._setLayersByCategory();
    this._setButtons();

    L.Control.EasyBar.prototype.initialize.call(this, this._buttons);
  },

  _setLayersByCategory: function () {
    const layers = this.options.layer.getLayers();
    const layersByCategory = {};

    layers.forEach((layer) => {
      let category = layer.options[this.options.categoryKey];

      if (!this.options.categories.includes(category)) {
        category = this.options.fallbackCategoryName;
      }

      if (Object.keys(layersByCategory).includes(category)) {
        layersByCategory[category].push(layer);
      } else {
        layersByCategory[category] = [];
        layersByCategory[category].push(layer);
      }
    });

    this._layersByCategory = layersByCategory;
  },

  _setButtons: function () {
    const _layersByCategory = this._layersByCategory;
    const categories = Object.keys(_layersByCategory);

    const buttons = categories.map((category) => {
      const options = {
        categoryName: category,
        categoryLayers: this._layersByCategory[category],
        clusterLayer: this.options.layer,
        color: this._getColor(category),
      };
      return categoryControlButton(options);
    });

    this._buttons = buttons;
  },

  _getColor: function (category) {
    const defaultColor = this.options.categoryOptions.defaultColor;
    const colorMap = this.options.categoryOptions.colorMap;

    if (Object.hasOwn(colorMap, category)) {
      return colorMap[category];
    }

    return defaultColor;
  },
});

export const categoryControl = (options) => new CategoryControl(options);
