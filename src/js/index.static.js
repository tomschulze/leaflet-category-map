"use strict";
import { categoryMap } from "./LeafletCategoryMap/index.js";
import "@styles/main/main.scss";
import "@styles/custom/custom.scss";
import { options } from "./config.js";

import data from "../../dev/data/data.json";

const map = categoryMap("leaflet-category-demo", options);
map.addMarkerLayer(data);
