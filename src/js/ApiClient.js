class ApiClient {
  _host = null;

  constructor() {
    this._setHost();
  }

  get(endpoint, callback) {
    let apiUrl = this._host + endpoint;
    const url = new URL(apiUrl);

    if (!url) {
      throw new Error(`Invalid url provided: ${url}`);
    }

    let xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
      if (xhr.status === 200) {
        const data = JSON.parse(xhr.responseText);
        callback(data);
      } else {
        console.error(
          "XHR Request Error for url " + url + ": " + xhr.responseText,
        );
      }
    };
    xhr.send();
  }
  _setHost() {
    this._host = window.location.origin;
  }
}

export { ApiClient };
