import { divIcon, point } from "leaflet";
import worldTopoJson from "@assets/world.topo.json";
import { customPopupHtml } from "./CustomPopupHtml.js";

const leafletOptions = {
  zoomControl: false,
  center: [30, 0],
  zoom: 2.5,
  zoomSnap: 0.25,
  minZoom: 1,
  maxZoom: 5,
  gestureHandling: true,
  attributionControl: false,
};

const clusterOptions = {
  spiderfyOnMaxZoom: true,
  showCoverageOnHover: false,
  zoomToBoundsOnClick: true,
  iconCreateFunction: function (cluster) {
    const icon = divIcon({
      html: "<div><span><b>" + cluster.getChildCount() + "</b></span></div>",
      iconSize: point(40, 40),
      className: "marker-cluster-default marker-cluster",
    });
    return icon;
  },
};

// for topojson
const svgOptions = {
  color: "#e6e6e6",
  fillColor: "#fff",
  weight: 1,
  fillOpacity: 1,
};

// https://stackoverflow.com/questions/1053902
const slugify = (text) => {
  return text
    .toLowerCase()
    .replace(/[^\w ]+/g, "")
    .replace(/ +|\_+/g, "-");
};

// to extract props (passed to popuphtml), categories and coordinates from data
const extractors = {
  categoryExtractor: (el) => slugify(el.category),
  coordinateExtractor: (el) => [el.coordinates.lat, el.coordinates.lon],
  propsExtractor: (el) => {
    return {
      abstract: el.abstract,
      headline: el.headline,
      leadParagraph: el.leadParagraph,
      pubDate: el.pubDate,
      webUrl: el.webUrl,
      location: el.glocations[0],
      category: el.category,
    };
  },
};

const categoryOptions = {
  colorMap: {
    "rock-climbing": "#58508d",
    "ice-climbing": "#bc5090",
    "parkour-sport": "#ff6361",
    "travel-and-vacations": "#ffa600",
  },
};

export const options = {
  topoJsonData: worldTopoJson,
  leafletOptions,
  svgOptions,
  clusterOptions,
  categoryOptions,
  popupHtml: customPopupHtml(),
  // TODO name differently? 'extractors' sounds very complicated
  extractors,
};
