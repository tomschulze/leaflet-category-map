import { readFile } from "fs/promises";

const slugify = (text) => {
  return text
    .toLowerCase()
    .replace(/[^\w ]+/g, "")
    .replace(/ +|\_+/g, "-");
};

const getCountBy = (keyword, data) => {
  return data.filter((el) => el.category == keyword).length;
};

(async () => {
  const pathUrl = new URL("./data/data_raw.json", import.meta.url);
  const fileContents = await readFile(pathUrl, {
    encoding: "utf-8",
  }).catch((error) => {
    if (error.message.startsWith("ENOENT")) {
      console.log(
        `file '${pathUrl.pathname}' not found. did you fetch the data yet?`,
      );
    } else {
      console.error(error.message);
    }
    process.exit(1);
  });

  const data = JSON.parse(fileContents);

  if (data && data.length == 0) {
    console.log("File is empty.");
    process.exit(1);
  }

  const filteredKeyword = data.filter(
    (datapoint) => datapoint.category != null,
  );

  const uniqueKeywords = [
    ...new Set(filteredKeyword.map((datapoint) => datapoint.category)),
  ];

  console.log(
    `${filteredKeyword.length}/${data.length} have a category assigned.`,
  );
  console.log(`There are ${uniqueKeywords.length} unique categories.`);
  console.log(`\n`);
  console.log(
    "Categories w/ category ids for adapting css and providing color map:",
  );
  for (const keyword of uniqueKeywords) {
    console.log(`${keyword}: ${slugify(keyword)}`);
  }

  const counts = uniqueKeywords.map((keyword) => ({
    [keyword]: getCountBy(keyword, filteredKeyword),
  }));

  console.log("\nCounts by category:");
  console.log(counts);

  console.log("\nSample article:");
  console.log(filteredKeyword[0]);
})();
