import express from "express";
import webpack from "webpack";
import webpackDevMiddleware from "webpack-dev-middleware";
import { readFile } from "fs/promises";

import config from "../webpack.config.js";

const loadData = async () => {
  const path = new URL("data/data.json", import.meta.url);
  const data = await readFile(path).catch((error) => {
    if (error.message.startsWith("ENOENT")) {
      console.log(
        `file '${pathUrl.pathname}' not found. did you fetch the data yet?`,
      );
    } else {
      console.error(error.message);
    }
    process.exit(1);
  });

  return data;
};

(async () => {
  const data = await loadData();

  const app = express();
  const port = 3000;

  config.mode = "development";
  const compiler = webpack(config);

  app.use(
    webpackDevMiddleware(compiler, {
      publicPath: config.output.publicPath,
    }),
  );

  app.get("/api/articles", (req, res) => {
    res.send(data);
  });

  app.listen(port, () => {
    console.log(`Example app listening on http://0.0.0.0:${port}`);
  });
})();
