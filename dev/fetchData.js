import { writeFileSync } from "fs";

import { NYTimesClient } from "./clients/NYTimesClient.js";
import { NominatimClient } from "./clients/NominatimClient.js";

import { config } from "dotenv";
config();

const FILE_PATH_RAW = new URL("./data/data_raw.json", import.meta.url);
const FILE_PATH_PROCESSED = new URL("./data/data.json", import.meta.url);

const keywords = [
  '"SPORTS CLIMBING"',
  '"Rock Climbing"',
  '"Ice Climbing"',
  '"Parkour (Sport)"',
];

const nyTimesOptions = {
  apiKey: process.env.API_KEY_NYTIMES,
  maxPage: 10,
  queryParams: {
    fq: `subject:(${keywords.join(",")}) AND _exists_:glocations AND document_type:(article) AND NOT type_of_material.contains:"Obituary"`,
  },
};

const nominatimOptions = {
  queryParams: {
    format: "geocodejson",
  },
};

const nyTimesClient = new NYTimesClient(nyTimesOptions);
const nominatimClient = new NominatimClient(nominatimOptions);

const retrieveCoordinates = (article) => {
  const { glocations } = article;
  return nominatimClient.getCoordinates(glocations[0]);
};

(async () => {
  console.log("retrieving articles from NY Times API...");
  let articles = await nyTimesClient.getResults();
  let geocodedArticles = [];

  console.log("geocoding locations via the OSM Nominatim API...");

  for (const article of articles) {
    let result = await retrieveCoordinates(article);
    if (!result) {
      console.log(
        `no coordinates retrieved for article with headline '${article.headline}'`,
      );
      continue;
    }
    const coordinates = { coordinates: { lat: result.lat, lon: result.lon } };
    geocodedArticles.push(Object.assign({}, article, coordinates));
  }

  const geocodedArticlesProcessed = geocodedArticles.filter(
    (article) => article.category != null,
  );

  console.log("saving all articles  to " + FILE_PATH_RAW);
  writeFileSync(FILE_PATH_RAW, JSON.stringify(geocodedArticles));
  console.log(
    "saving articles with valid categories to " + FILE_PATH_PROCESSED,
  );
  writeFileSync(FILE_PATH_PROCESSED, JSON.stringify(geocodedArticlesProcessed));
})();
