import querystring from "querystring";
import { setTimeout } from "timers/promises";

export class NominatimClient {
  constructor({ queryParams }) {
    this.baseUrl = "https://nominatim.openstreetmap.org/search";
    this.queryParams = queryParams;
    this.headers = new Headers({
      "User-Agent": "nominatim client for geocoding NY Times news articles",
    });
  }

  _buildUrl(query) {
    this.queryParams.q = query;
    const params = querystring.stringify(this.queryParams);
    return this.baseUrl + "?" + params;
  }

  _extractCoordinates(results) {
    if (results.features.length == 0) {
      return null;
    }

    const coordinates = results.features[0].geometry.coordinates;

    return {
      lat: coordinates[1],
      lon: coordinates[0],
    };
  }

  async getCoordinates(query) {
    const url = this._buildUrl(query);
    let response = null;

    try {
      response = await setTimeout(2000, fetch(url, { headers: this.headers }));
    } catch (error) {
      console.log({ url, error });
      return null;
    }

    const { status, statusText } = response;

    if (status != 200) {
      console.log(status, statusText, url);
      return null;
    }

    const json = await response.json();
    return this._extractCoordinates(json);
  }
}
