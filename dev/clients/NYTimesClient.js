import querystring from "querystring";
import { setTimeout } from "timers/promises";

export class NYTimesClient {
  constructor({ apiKey, queryParams, maxPage }) {
    this.apiKey = apiKey;
    this.queryParams = queryParams;
    this.maxPage = maxPage > 100 ? 100 : maxPage;
    this.initPage = 0;
    this.baseUrl = "https://api.nytimes.com/svc/search/v2/articlesearch.json";
  }

  _buildUrl(page) {
    this.queryParams["api-key"] = this.apiKey;
    this.queryParams.page = page;
    const params = querystring.stringify(this.queryParams);
    return this.baseUrl + "?" + params;
  }

  _processDocument = (doc) => {
    const glocations = doc.keywords
      .filter((keyword) => {
        if (keyword.name == "glocations") {
          return true;
        }
        return false;
      })
      .map((keyword) => keyword.value);

    const keywordByRank = (rank) => {
      let keyword = doc.keywords.filter((keyword) => {
        if (keyword.rank == rank && keyword.name == "subject") {
          return true;
        }
        return false;
      });

      if (keyword.length > 0) {
        return keyword[0].value;
      }

      return null;
    };

    return {
      abstract: doc.abstract,
      headline: doc.headline.main,
      leadParagraph: doc.lead_paragraph,
      pubDate: doc.pub_date,
      webUrl: doc.web_url,
      glocations,
      keywordRank1: keywordByRank(1),
      keywordRank2: keywordByRank(2),
      documentType: doc.document_type,
      typeOfMaterial: doc.type_of_material,
      category: keywordByRank(1) ? keywordByRank(1) : keywordByRank(2),
    };
  };

  async _getResult(page) {
    const url = this._buildUrl(page);
    const response = await fetch(url);
    const { status } = response;
    const json = await response.json();

    return {
      status,
      json,
    };
  }

  async getResults() {
    let currentPage = this.initPage;
    const results = [];

    while (true) {
      if (currentPage === this.maxPage) {
        break;
      }

      let response = await setTimeout(12 * 1000, this._getResult(currentPage));
      console.log(`received page ${currentPage + 1}/${this.maxPage}`);

      if (response.status === 401) {
        console.log(
          "no data received. status 401. did you provide your ny times api key?",
        );
        break;
      }

      if (response.status === 429) {
        console.log("per day api limit reached...");
        break;
      }

      if (!response.json.response) {
        console.log("no data received, status: " + response.status);
        break;
      }

      let docs = response.json.response.docs;

      docs = docs.map(this._processDocument);
      results.push(...docs);

      currentPage++;
    }

    return results;
  }
}
