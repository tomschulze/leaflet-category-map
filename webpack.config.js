import path from "path";
import { fileURLToPath } from "url";

const __dirname = path.dirname(fileURLToPath(import.meta.url));

import MiniCssExtractPlugin from "mini-css-extract-plugin";
import HtmlWebpackPlugin from "html-webpack-plugin";
import pkg from "inject-body-webpack-plugin";
const InjectBodyPlugin = pkg.default;

const htmlWebpackPlugin = new HtmlWebpackPlugin({
  title: "Leaflet Category Demo",
});

const injectBodyPlugin = new InjectBodyPlugin({
  content: '<div id="leaflet-category-demo"></div>',
});

const miniCssExtractPlugin = new MiniCssExtractPlugin({
  filename: "css/bundle.maps.css",
});

const entryPoint = process.env.USE_STATIC_BUILD
  ? "index.static.js"
  : "index.js";

export default {
  entry: path.resolve(__dirname, "src", "js", entryPoint),
  resolve: {
    alias: {
      "@assets": path.resolve(__dirname, "src", "assets"),
      "@styles": path.resolve(__dirname, "src", "scss"),
    },
  },
  devServer: {
    static: "./dist/",
  },
  plugins: [miniCssExtractPlugin, htmlWebpackPlugin, injectBodyPlugin],
  module: {
    rules: [
      {
        test: /\.scss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              url: {
                filter: (url) => {
                  if (url.startsWith("@assets")) {
                    return true;
                  }
                  return false;
                },
              },
            },
          },
          "sass-loader",
        ],
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/i,
        type: "asset/resource",
      },
      {
        test: /\.(svg)$/i,
        type: "asset/source",
      },
      {
        test: /\.(json)$/i,
        type: "json",
      },
    ],
  },
  output: {
    filename: "js/bundle.maps.js",
    path: path.resolve(__dirname, "public"),
    clean: true,
    publicPath: "",
    assetModuleFilename: "images/[hash][ext][query]",
  },
};
